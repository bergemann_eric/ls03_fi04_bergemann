package formaendern_meins;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import java.awt.Color;
import javax.swing.UIManager;
import java.awt.SystemColor;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import java.awt.Font;
import java.awt.CardLayout;
import java.awt.GridLayout;
import javax.swing.SwingConstants;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.sun.javafx.scene.paint.GradientUtils.Parser;
import javax.swing.JColorChooser;

import java.awt.FlowLayout;
import net.miginfocom.swing.MigLayout;
import java.awt.GridBagLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JFormattedTextField;

public class form_kopie {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					form_kopie window = new form_kopie();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public form_kopie() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.LIGHT_GRAY);
		frame.getContentPane().setLayout(null);
		
		JLabel lblText = new JLabel("Mustertext");
		lblText.setHorizontalAlignment(SwingConstants.CENTER);
		lblText.setBounds(0, 38, 362, 25);
		frame.getContentPane().add(lblText);
		
		JLabel lblHintergrundfarbendern = new JLabel("Hintergrundfarbe \u00E4ndern");
		lblHintergrundfarbendern.setBounds(0, 91, 362, 25);
		frame.getContentPane().add(lblHintergrundfarbendern);
		
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getContentPane().setBackground(Color.RED);
			}
		});
		btnRot.setBounds(0, 120, 115, 25);
		frame.getContentPane().add(btnRot);
		
		
		JButton btnGrn = new JButton("Gr\u00FCn");
		btnGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getContentPane().setBackground(Color.GREEN);
			}
		});
		btnGrn.setBounds(125, 120, 115, 25);
		frame.getContentPane().add(btnGrn);
		
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getContentPane().setBackground(Color.BLUE);
			}
		});
		btnBlau.setBounds(247, 120, 115, 25);
		frame.getContentPane().add(btnBlau);
		
		JButton btnGelb = new JButton("Gelb");
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getContentPane().setBackground(Color.YELLOW);
			}
		});
		btnGelb.setBounds(0, 147, 115, 25);
		frame.getContentPane().add(btnGelb);
		
		JButton btnStandardfarbe = new JButton("Standardfarbe");
		btnStandardfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.getContentPane().setBackground(Color.LIGHT_GRAY);
			}
		});
		btnStandardfarbe.setBounds(125, 147, 115, 25);
		frame.getContentPane().add(btnStandardfarbe);
		
		JButton btnFarbeWhlen = new JButton("Farbe w\u00E4hlen");
		btnFarbeWhlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			//	frame.getContentPane().setBackground(JColorChooser.CHOOSER_PANELS_PROPERTY);
			}
		});
		btnFarbeWhlen.setBounds(247, 147, 115, 25);
		frame.getContentPane().add(btnFarbeWhlen);
		
		JFormattedTextField frmtdtxtfldText = new JFormattedTextField();
		frmtdtxtfldText.setText("Hier Text eingeben");
		frmtdtxtfldText.setBounds(0, 233, 362, 22);
		frame.getContentPane().add(frmtdtxtfldText);
		
		JLabel lblTextFormatieren = new JLabel("Text formatieren");
		lblTextFormatieren.setBounds(0, 173, 362, 25);
		frame.getContentPane().add(lblTextFormatieren);
		
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setFont(new Font("Courier New", Font.PLAIN, 12));
			}
		});
		btnCourierNew.setBounds(247, 199, 115, 25);
		frame.getContentPane().add(btnCourierNew);
		
		JButton btnComicSansMs = new JButton("Comic Sans MS");
		btnComicSansMs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
			}
		});
		btnComicSansMs.setBounds(119, 199, 121, 25);
		frame.getContentPane().add(btnComicSansMs);
		
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setFont(new Font("Arial", Font.PLAIN, 12));
			}
		});
		btnArial.setBounds(0, 199, 115, 25);
		frame.getContentPane().add(btnArial);
		
		JButton btnLabelBeschreiben = new JButton("Label beschreiben");
		btnLabelBeschreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setText(frmtdtxtfldText.getText());
			}
		});
		btnLabelBeschreiben.setBounds(0, 259, 175, 25);
		frame.getContentPane().add(btnLabelBeschreiben);
		
		JButton btnLabelInhateLschen = new JButton("Label Inhate l\u00F6schen");
		btnLabelInhateLschen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setText("");
			}
		});
		btnLabelInhateLschen.setBounds(187, 259, 175, 25);
		frame.getContentPane().add(btnLabelInhateLschen);
		
		JLabel lblSchriftfarbendern = new JLabel("Schriftfarbe \u00E4ndern");
		lblSchriftfarbendern.setBounds(0, 286, 362, 25);
		frame.getContentPane().add(lblSchriftfarbendern);
		
		JButton btn_Rot = new JButton("Rot");
		btn_Rot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setForeground(Color.RED);
			}
		});
		btn_Rot.setBounds(0, 311, 115, 25);
		frame.getContentPane().add(btn_Rot);
		
		JButton btn_Blau = new JButton("Blau");
		btn_Blau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setForeground(Color.BLUE);
			}
		});
		btn_Blau.setBounds(125, 311, 115, 25);
		frame.getContentPane().add(btn_Blau);
		
		JButton btn_Schwarz = new JButton("Schwarz");
		btn_Schwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setForeground(Color.BLACK);
			}
		});
		btn_Schwarz.setBounds(247, 311, 115, 25);
		frame.getContentPane().add(btn_Schwarz);
		
		JLabel lblSchriftgreVerndern = new JLabel("Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblSchriftgreVerndern.setBounds(0, 337, 362, 25);
		frame.getContentPane().add(lblSchriftgreVerndern);
		
		JButton button = new JButton("+");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setFont(new Font(lblText.getFont().getFontName(), Font.PLAIN, lblText.getFont().getSize()+1)); 
			}
		});
		button.setBounds(0, 362, 175, 25);
		frame.getContentPane().add(button);
		
		JButton button_1 = new JButton("-");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setFont(new Font(lblText.getFont().getFontName(), Font.PLAIN, lblText.getFont().getSize()-1)); 
			}
		});
		button_1.setBounds(187, 362, 175, 25);
		frame.getContentPane().add(button_1);
		
		JLabel lblTextausrichtung = new JLabel("Textausrichtung");
		lblTextausrichtung.setBounds(0, 391, 362, 25);
		frame.getContentPane().add(lblTextausrichtung);
		
		JButton btnLinksbndig = new JButton("Linksb\u00FCndig");
		btnLinksbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnLinksbndig.setBounds(0, 417, 115, 25);
		frame.getContentPane().add(btnLinksbndig);
		
		JButton btnZentriert = new JButton("Zentriert");
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnZentriert.setBounds(125, 417, 115, 25);
		frame.getContentPane().add(btnZentriert);
		
		JButton btnRechtsbndig = new JButton("Rechtsb\u00FCndig");
		btnRechtsbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblText.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnRechtsbndig.setBounds(247, 417, 115, 25);
		frame.getContentPane().add(btnRechtsbndig);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btnExit.setBounds(0, 455, 362, 78);
		frame.getContentPane().add(btnExit);
		frame.setBounds(100, 100, 380, 580);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
