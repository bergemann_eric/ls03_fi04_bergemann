package view.menue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import controller.AlienDefenceController;
import controller.GameController;
import controller.LevelController;
import model.Level;
import model.User;
import model.persistanceDB.PersistanceDB;
import view.StartAlienDefence;
import view.game.GameGUI;
import view.menue.MainMenue;

import javax.swing.JTextPane;
import java.awt.Cursor;
import java.awt.Component;
import java.awt.ComponentOrientation;

@SuppressWarnings("serial")
public class LevelChooser extends JPanel {

	private LevelController lvlControl;
	private LeveldesignWindow leveldesignWindow;
	private JTable tblLevels;
	private DefaultTableModel jTableData;
	private AlienDefenceController alienDefenceController;
	private LevelController levelController;
	private MainMenue main;
	private GameController gameController;

	/**
	 * Create the panel.
	 * 
	 * @param leveldesignWindow
	 */
	public LevelChooser(LevelController lvlControl, LeveldesignWindow leveldesignWindow, AlienDefenceController aliendefencecontroller, String Button) {
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		setForeground(Color.GREEN);
		setBackground(Color.BLACK);
		this.lvlControl = lvlControl;
		this.leveldesignWindow = leveldesignWindow;
		this.alienDefenceController = aliendefencecontroller;

		setLayout(new BorderLayout());

		JPanel pnlButtons = new JPanel();
		pnlButtons.setForeground(Color.YELLOW);
		pnlButtons.setBackground(Color.BLACK);
		add(pnlButtons, BorderLayout.SOUTH);

		JButton btnNewLevel = new JButton("Neues Level");
		btnNewLevel.setForeground(Color.BLACK);
		btnNewLevel.setBackground(Color.GREEN);
		btnNewLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnNewLevel_Clicked();
			}
		});
		pnlButtons.add(btnNewLevel);

		JButton btnUpdateLevel = new JButton("ausgew\u00E4hltes Level bearbeiten");
		btnUpdateLevel.setBackground(Color.GREEN);
		btnUpdateLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUpdateLevel_Clicked();
			}
		});
		pnlButtons.add(btnUpdateLevel);

		JButton btnDeleteLevel = new JButton("Level l\u00F6schen");
		btnDeleteLevel.setBackground(Color.GREEN);
		btnDeleteLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDeleteLevel_Clicked();
			}
		});
		pnlButtons.add(btnDeleteLevel);
		
		JTextPane textPane = new JTextPane();
		textPane.setEnabled(false);
		textPane.setEditable(false);
		textPane.setBackground(Color.BLACK);
		textPane.setText("        ");
		pnlButtons.add(textPane);
		
		//if(check==false) {
		JButton btnSpielen = new JButton("Spielen");
		btnSpielen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				//User user = new PersistanceDB().getUserPersistance().readUser(tfdLogin.getText());
				
				User user = new User(2100, "", "");
				
				int i = findLevelID();

				Thread t = new Thread("GameThread") {

					@Override
					public void run() {

						//GameController gameController = alienDefenceController.startGame(arrLevel.get(i), user);
						//new GameGUI(gameController).start();
						
						//GameController gameController = new GameController(alienDefenceController.getLevelController().readLevel(findLevelID()-1), user, alienDefenceController);  //.startGame(main.getArrlevel().get(i), user);
						//gameController = alienDefenceController.startGame(levelController.readLevel(i-1), user);
						//new GameGUI(gameController).start();
						btnStartLevel_Clicked();
					}
				};
				t.start();
				
			}
		});
		
		
		btnSpielen.setBackground(Color.ORANGE);
		pnlButtons.add(btnSpielen);

		JLabel lblLevelauswahl = new JLabel("Levelauswahl");
		lblLevelauswahl.setForeground(Color.GREEN);
		lblLevelauswahl.setBackground(Color.BLACK);
		lblLevelauswahl.setFont(new Font("Arial", Font.BOLD, 18));
		lblLevelauswahl.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblLevelauswahl, BorderLayout.NORTH);

		JScrollPane spnLevels = new JScrollPane();
		add(spnLevels, BorderLayout.CENTER);

		tblLevels = new JTable();
		tblLevels.setForeground(Color.YELLOW);
		tblLevels.setBackground(Color.BLACK);
		tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		spnLevels.setViewportView(tblLevels);

		this.updateTableData();
		
		if(Button.equals("Spielen")) {
			btnUpdateLevel.setVisible(false);
			btnDeleteLevel.setVisible(false);
	        btnNewLevel.setVisible(false);
	        btnSpielen.setVisible(true);
		}
		else if(Button.equals("Leveleditor")) {
			btnSpielen.setVisible(false);
			btnUpdateLevel.setVisible(true);
			btnDeleteLevel.setVisible(true);
	        btnNewLevel.setVisible(true);
		}
	}

	private String[][] getLevelsAsTableModel() {
		List<Level> levels = this.lvlControl.readAllLevels();
		String[][] result = new String[levels.size()][];
		int i = 0;
		for (Level l : levels) {
			result[i++] = l.getData();
		}
		return result;
	}

	public void updateTableData() {
		this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
		this.tblLevels.setModel(jTableData);
	}

	public void btnNewLevel_Clicked() {
		this.leveldesignWindow.startLevelEditor();
	}

	public void btnUpdateLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.leveldesignWindow.startLevelEditor(level_id);
	}

	public void btnDeleteLevel_Clicked() {
		int level_id = Integer.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.lvlControl.deleteLevel(level_id);
		this.updateTableData();
	}
	public void btnStartLevel_Clicked() {
		
		  Thread t = new Thread("GameThread") {
	            @Override
	            public void run() {
	                User user = new User(1, "", "");

	                List<Level> arrLevel = lvlControl.readAllLevels();

	                int selectedRow;
	                selectedRow = tblLevels.getSelectedRow();
	                GameController gameController = alienDefenceController.startGame(arrLevel.get(selectedRow), user);
	                new GameGUI(gameController).start();
	            }
	        };
	        t.start();
	
	}
	public int findLevelID() {
		int level_id = Integer.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		System.out.println(level_id);
		return level_id;
	}
}
